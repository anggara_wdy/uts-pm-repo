import 'package:flutter/material.dart';
import 'package:uts_pemrograman_mobile/detail.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.deepOrange,
      ),
      home: MyHomePage(title: 'Product Navigation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Widget buildCard1() => FlatButton(
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailApp(
                imageName: 'assets/image/valorant.jpeg',
                name: 'Valorant',
                desc: 'Desc',
                price: 'Free',
              )
          )
      );
    },
    child: Card(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset('assets/image/valorant.jpeg', fit: BoxFit.cover),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text("Valorant", style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                  Text("FPS Game"),
                  Text("Price: Free", textAlign: TextAlign.center)
                ],
              ),
            )
          )
        ],
      ),
    ),
  );

  Widget buildCard2() => FlatButton(
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailApp(
                imageName: 'assets/image/dota2.jpg',
                name: 'Dota 2',
                desc: 'RTS Game',
                price: 'Free',
              )
          )
      );
    },
    child: Card(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset('assets/image/dota2.jpg', fit: BoxFit.cover),
          ),
          Expanded(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text("Dota 2", style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                    Text("RTS Game"),
                    Text("Price: Free", textAlign: TextAlign.center)
                  ],
                ),
              )
          )
        ],
      ),
    ),
  );

  Widget buildCard3() => FlatButton(
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailApp(
                imageName: 'assets/image/csgo.jpg',
                name: 'CS:GO',
                desc: 'FPS Game',
                price: '15 USD',
              )
          )
      );
    },
    child: Card(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset('assets/image/csgo.jpg', fit: BoxFit.cover),
          ),
          Expanded(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text("CS:GO", style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                    Text("FPS Game"),
                    Text("Price: 15 USD", textAlign: TextAlign.center)
                  ],
                ),
              )
          )
        ],
      ),
    ),
  );

  Widget buildCard4() => FlatButton(
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailApp(
                imageName: 'assets/image/apex.jpg',
                name: 'Apex Legends',
                desc: 'FPS Battle Royal Game',
                price: 'Free',
              )
          )
      );
    },
    child: Card(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset('assets/image/apex.jpg', fit: BoxFit.cover),
          ),
          Expanded(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text("Apex Legends", style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                    Text("FPS Battle Royal Game"),
                    Text("Price: Free", textAlign: TextAlign.center)
                  ],
                ),
              )
          )
        ],
      ),
    ),
  );

  Widget buildCard5() => FlatButton(
    onPressed: () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => DetailApp(
                imageName: 'assets/image/genshin.jpg',
                name: 'Genshin Impact',
                desc: 'MMORP Game',
                price: '20 USD',
              )
          )
      );
    },
    child: Card(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Image.asset('assets/image/genshin.jpg', fit: BoxFit.cover),
          ),
          Expanded(
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Text("Genshin Impact", style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                    Text("MMORP Battle Royal Game"),
                    Text("Price: 20 USD", textAlign: TextAlign.center)
                  ],
                ),
              )
          )
        ],
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: ListView(
        padding: EdgeInsets.all(10),
        children: [
          buildCard1(),
          buildCard2(),
          buildCard3(),
          buildCard4(),
          buildCard5(),
        ],
      )
    );
  }
}
