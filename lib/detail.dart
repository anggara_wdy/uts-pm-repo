import 'package:flutter/material.dart';

class DetailApp extends StatefulWidget {

  final String imageName;
  final String name;
  final String desc;
  final String price;

  const DetailApp({Key key, this.imageName, this.name, this.desc, this.price}) : super(key: key);

  @override
  DetailAppState createState() => DetailAppState();
}

class DetailAppState extends State<DetailApp> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail'),
        automaticallyImplyLeading: true,
      ),
      body: ListView(
        children: [
          Image.asset(this.widget.imageName, fit: BoxFit.cover),
          Column(
            children: [
              SizedBox(height: 10),
              Text(this.widget.name, style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center),
              SizedBox(height: 10),
              Text(this.widget.desc),
              SizedBox(height: 10),
              Text('Price : '+this.widget.price)
            ],
          )
        ],
      ),
    );
  }



}